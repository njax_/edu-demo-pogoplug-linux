import nmap

def main():
    cache_files()

    pogo_ip = search_for_pogo_plug()

    serve_files()


def cache_files():
    pass


def search_for_pogo_plug():
    current_network = '192.168.2.0/24'
    nm = nmap.PortScanner()
    # nm.scan(hosts='192.168.2.0/24', arguments='-sn')
    nm.scan(hosts=current_network, arguments='-sS -p T:22')

    # hosts_list = [(x, nm[x]['addresses']['mac']) for x in nm.all_hosts()]
    # hosts_list = [(ip, list(nm[ip]['vendor'].values())[0], list(nm[ip]['vendor'].keys())[0]) for ip in nm.all_hosts()]
    hosts_list = [(ip, nm[ip]) for ip in nm.all_hosts()]

    pogo_plugs = filter_pogoplugs(hosts_list)

    pogo_plugs = collect_ip_and_mac(pogo_plugs)

    if len(pogo_plugs) == 0:
        print("Error: Unable to find any PogoPlugs on this network, ")
    if len(pogo_plugs) > 2:
        print("The following PogoPlugs were found on your network.")
        print("Please select the one you wish to operate on:")
        print("")
        [print(x['mac']) for x in pogo_plugs]
        print("")

    return pogo_plugs[0][0]


def filter_pogoplugs(host_list):
    filtered_list = []

    for host in host_list:
        d = host[1]

        if 'vendor' in d:
            for _, value in d['vendor'].items():
                if 'Cloud Engines' in value:
                    filtered_list.append(host)
                    break

    return filtered_list


def collect_ip_and_mac(host_list):
    return [(ip, list(obj['vendor'].keys())[0]) for ip, obj in host_list]
