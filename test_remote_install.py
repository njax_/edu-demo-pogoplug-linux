import pytest

from remote_install import search_for_pogo_plug, collect_ip_and_mac, filter_pogoplugs

sample_hosts = [
    ('192.168.1.1', {'hostnames': [{'name': 'device1.example.com', 'type': 'PTR'}], 'addresses': {'ipv4': '192.168.1.1', 'mac': '00:00:00:00:00:01'}, 'vendor': {'00:00:00:00:00:01': 'Other Vendor'}, 'status': {'state': 'up', 'reason': 'arp-response'}}),
    ('192.168.1.2', {'hostnames': [{'name': 'device2.example.com', 'type': 'PTR'}], 'addresses': {'ipv4': '192.168.1.2', 'mac': '00:00:00:00:00:02'}, 'vendor': {'00:00:00:00:00:02': 'Cloud Engines'}, 'status': {'state': 'up', 'reason': 'arp-response'}}),
    ('192.168.1.3', {'hostnames': [{'name': 'device3.example.com', 'type': 'PTR'}], 'addresses': {'ipv4': '192.168.1.3', 'mac': '00:00:00:00:00:03'}, 'vendor': {'00:00:00:00:00:03': 'The Vendor'}, 'status': {'state': 'up', 'reason': 'arp-response'}})
]

sample_clean_hosts = [
    ('192.168.1.1', '00:00:00:00:00:01'),
    ('192.168.1.2', '00:00:00:00:00:02'),
    ('192.168.1.3', '00:00:00:00:00:03')
]

# def test_search_for_pogo_plug():
# 	pogo_plug_ip = search_for_pogo_plug()
# 	assert pogo_plug_ip == "192.168.2.13"

def test_collect_ip_and_mac_will_translate_the_horrible_frameworks_data_into_something_usable():
    output = collect_ip_and_mac(sample_hosts)

    assert output[0][0] == '192.168.1.1'
    assert output[0][1] == '00:00:00:00:00:01'
    assert output[1][0] == '192.168.1.2'
    assert output[1][1] == '00:00:00:00:00:02'
    assert output[2][0] == '192.168.1.3'
    assert output[2][1] == '00:00:00:00:00:03'

def test_filter_pogoplugs():
    filtered_pogos = filter_pogoplugs(sample_hosts)

    assert len(filtered_pogos) == 1
    assert filtered_pogos[0][0] == '192.168.1.2'
    assert filtered_pogos[0][1]['vendor'] == {'00:00:00:00:00:02': 'Cloud Engines'}
