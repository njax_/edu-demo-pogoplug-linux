#!/usr/bin/env bash

LAPTOP_IP='192.168.2.2'   # REPLACE THIS with your local machine's IP address

mkdir tools
cd tools
wget https://projects.doozan.com/uboot/install_uboot_mtd0.sh
LAPTOP_URI="http:\/\/${LAPTOP_IP}:8080"

# Patch the installer so it downloads from your laptop/ desktop
sed -i "s/^VALID_UBOOT_MD5.*/VALID_UBOOT_MD5=${LAPTOP_URI}\/valid-uboot.md5/" install_uboot_mtd0.sh
sed -i "s/^MIRROR.*/MIRROR=${LAPTOP_URI}/" install_uboot_mtd0.sh
sed -i 's/^UBOOT_MTD0_BASE_URL.*/UBOOT_MTD0_BASE_URL=$MIRROR\/uboot.mtd0/' install_uboot_mtd0.sh
sed -i 's/^UBOOT_ENV_URL.*/UBOOT_ENV_URL=$MIRROR\/uboot.2016.05-tld-1.environment.img/' install_uboot_mtd0.sh

# Cache the files locally
wget https://jeff.doozan.com/uboot/valid-uboot.md5
wget https://download.doozan.com/uboot/files/uboot/uboot.mtd0.pinkpogo.davygravy-2012-02-20.kwb
wget https://download.doozan.com/uboot/files/uboot/uboot.mtd0.pinkpogo.davygravy-2012-02-20.kwb.md5
wget https://download.doozan.com/uboot/files/uboot/uboot.mtd0.pinkpogo.original.kwb
wget https://download.doozan.com/uboot/files/uboot/uboot.mtd0.pinkpogo.original.kwb.md5
# wget https://download.doozan.com/uboot/files/environment/uboot.environment
# wget https://download.doozan.com/uboot/files/environment/uboot.environment.md5
# Get the latest environment settings
wget https://bitly.com/1sMwD7b -O uboot.2016.05-tld-1.environment.bodhi.tar
tar -xvf uboot.2016.05-tld-1.environment.bodhi.tar
echo "e4d3c14175319d206cfefb2a624f00a7" > uboot.2016.05-tld-1.environment.img.md5

wget https://download.doozan.com/uboot/blparam
wget https://download.doozan.com/uboot/blparam.md5
wget https://download.doozan.com/uboot/nanddump
wget https://download.doozan.com/uboot/nanddump.md5
wget https://download.doozan.com/uboot/nandwrite
wget https://download.doozan.com/uboot/nandwrite.md5
wget https://download.doozan.com/uboot/flash_erase
wget https://download.doozan.com/uboot/flash_erase.md5
wget https://download.doozan.com/uboot/fw_printenv
wget https://download.doozan.com/uboot/fw_printenv.md5
wget https://download.doozan.com/uboot/fw_env.config
wget https://download.doozan.com/uboot/fw_env.config.md5

wget https://bitly.com/2ySaD0T -O uboot.2017.07-tld-1.pogo_e02.mtd0.kwb.tar
tar -xvf uboot.2017.07-tld-1.pogo_e02.mtd0.kwb.tar
mv uboot.2017.07-tld-1.pogo_e02.mtd0.kwb uboot.mtd0.pinkpogo.bodhi-2017-07-01.kwb
echo "873c23936f3637792873ee77d3dee70d" > uboot.mtd0.pinkpogo.bodhi-2017-07-01.kwb.md5
sed -i "s/^e84a5fd0a0205bb79aed07c3c6fbd145.*/e84a5fd0a0205bb79aed07c3c6fbd145 pinkpogo davygravy-2012-02-20/" valid-uboot.md5

echo "873c23936f3637792873ee77d3dee70d pinkpogo bodhi-2017-07-01-current" >> valid-uboot.md5

echo "873c23936f3637792873ee77d3dee70d" > uboot.mtd0.kwb.md5

echo "########################"
echo "# Almost Done          #"
echo "########################"
echo ""
echo "Files cached and ready to be pulled to the PogoPlug."
echo "On the PogoPlug's shell, stick in the usb drive you want to format for use as your linux disk.  Then run the below commands:"
echo ""
echo "Disclaimer: Flashing your PogoPlug carries a risk of breaking your device."
echo ""
echo "killall hbwd"
echo "/usr/sbin/fw_setenv usb_rootfstype ext3"

echo "cd /tmp"
echo "wget http://${LAPTOP_IP}:8080/install_uboot_mtd0.sh"
echo "chmod 0755 install_uboot_mtd0.sh"
echo "./install_uboot_mtd0.sh"
echo ""

echo "On your laptop/ Desktop, you need to serve this folder to your Pogo Plug over http."
echo "If python3 is already installed, just run this:"
echo ""
echo "python3 -m http.server 8080"
