# Installing Linux on a POGO-E02 Pogo Plug from Stock

Disclaimer: Flashing your PogoPlug carries a risk of breaking your device.

This guide assumes you're starting from a stock PogoPlug.  It should say POGO-E02 on the bottom; the unit can be pink or black --otherwise this is not the guide for you.  

So this post is a little late and makes a lot of sense before RPi3 came out.  We're basically putting Debian Linux onto an ARM-based board that was meant to just be a single purpose product, a USB-based NAS.  It's cool because it's hacking but in terms of features, you're honestly better off using an RPi 4 or 5 to be able to really saturate a gigabit ethernet port.  The HDD read speeds are limited to around 25Mb/s due to the capabilities of the old ARM CPU, which is much less than the 125Mb/s that my ethernet switch can handle.  Using encryption over the wire also slows things down so that's not good but technically if you're using it within your own LAN it shouldn't really impact things moving files over plain FTP.  

I plan to use mine for storing versioned VM artifacts.  I maintain a packer-based suite of VMs, and currently the latest 2 versions of each of my machine images just sits on my hypervisor.  When I roll out a new release, the 3rd most recent just goes away forever, which has never felt right.  Due to the slow upload rate, I don't think it's a good idea to handle the transfer within the same CI/ CD job doing the build, I like my jobs finished fast and VMs are obscenely time-consuming.  Probably what I'll do is add a script to the hypervisor to perform the transfer, and have a CI/ CD job kick off the transfer.  Maybe this is a good opportunity to package a Golang service as a deb package.  


## Tools

###### Required
- A linux machine with `fdisk`, `mke2fs`, and `python3`

###### Optional
- A USB - TTL adapter will be needed if anything goes wrong (not covered here)

## Overview: How the Hack Works

So there's two things, first the ARM board has serial ports that you can connect to using a USB-TTL serial adapter.  Using a program like `screen` you'll be able to hook into the PogoPlug, boot it up, and then see the standard output from the ARM Board's boot up process.  Once it's finished booting, it drops you right into a root shell!  It's actually a little too easy to call it a hack, but a good place to start if you've never had success digging around an embedded system before.

Ok, the second thing is that, they left ssh right open and the password leaked, it's 'ceadmin' so you don't even need to hook into the serial terminals (although if the procedure goes wrong, you'll need to in order to unbrick the thing).  Also it's fun so if you have one of these things you should definitely give it a try.  

###### So You Need a Fresh Debian Install

Once you're at a root shell, you're in linux kind of... but the OS is so stripped down that you can't even install a new program with apt-get (et. al.) as you would on a normal linux distribution.  This device, like many embedded solutions, is based around busy-box, a lightweight set of UNIX executables meant to be small and performant.  

###### So You Need to Update U-Boot

In order to boot up into linux, you need a bootloader.  The stock bootloader is old so you'll want to update it in order to boot into modern linux distros.  

In doing this project, I realized that it's the bootloader that has to be very meticulously written to the disk.  You're probably familiar with creating a bootable linux USB drive, and so you'll recall that files are not simply dragged and dropped into the usb drive.  Instead an image writing software is used (perhaps dd).  This is because the Master Boot Record needs to be written to the first sector of the disk to work, and the data isn't arrange as files, it's just the first 512 bytes of the disk.  

With this hack, we'll be writing our bootloader to the NAND flash disk, /dev/mtd0 (as opposed to say /dev/sda).  But we won't actually be flashing linux onto the same disk as our bootloader.  Instead we'll just have U-Boot look for a USB Drive with an ext4 partition with a volume label of "rootfs".  That way, when we need to install linux, we actually will just be drag and dropping the linux files onto the USB drive.  It will feel weird, but trust me it will actually work.  


## References

The information on how to do this is spreed out across a forum a various parts of the internet.  If you're unfamiliar with linux, Arm, bootloaders, etc. and are confused, don't worry, it's not you, it's just hard to follow because there are a lot of products using this Arm chipset (Marvell Kirkwood) and directions get conditional and out of hand very fast.  With that said, here is kind of the central [wiki](https://forum.doozan.com/read.php?2,23630) for collecting extra details related to the PogoPlug's Kirkland Chipset.  


## Shelling into the Stock PogoPlug

###### Find the IP

To find my PogoPlug on the network, I login to my router and pull up it's logs.  It prints DHCP reservations as the device boot up.  You could also try `sudo nmap -sn 192.168.1.0/24 | grep '00:25:..' -B2` changing `00:25:..` to the mac address found on the bottom of your PogoPlug.  

###### Configure your Laptop's SSH Connection

Since the device is so old, my mac wouldn't let me connect to the pogo plug without these configurations.  Change the IP to your PogoPlug's IP.  

(~/.ssh/config)
```
Host pogo
  HostName 192.168.1.50
  User root
  PasswordAuthentication yes
  KexAlgorithms +diffie-hellman-group1-sha1
  HostKeyAlgorithms=+ssh-rsa
```

When you run `ssh pogo` on your local machine, you'll be prompted for a password; it's `ceadmin`.  

If this fails, you'll need to hook in over the serial terminals.  To keep this guide lean, I'm referring you to [here](http://blog.qnology.com/2013/10/pogoplug-e02-v2-serial-connection.html).  


## Upgrading U-Boot

Ref: https://projects.doozan.com/uboot/

If you attempt to run the above reference's instructions, you'll find it doesn't work right.  This is because of the internet no longer using http.  The wget on the PogoPlug is from busy box which isn't compiled with https support.  

To work around this, we're just going to download the files on our laptops, and then serve those files to the Pogo Plug over http.  It's secure since all the files are transmitted to your private network over TLS, and if an adversary is able to intercept the http messages between your laptop and your Pogo Plug, it's already too late.  

One of the engineering values I'm most interested in is documentation.  I do a lot of projects and bounce between them often.  Sometimes years will go by before I need to return to, say, a k8s deployment project.  If I'm not thoughtful about how I leave these projects, it can take ages reverse engineering my work and getting back into them.  A good start addressing this problem is writing clean docs, but that's not really where I stop.  

After I've written my docs, I review it and come up with ways to pull things out of the document, with the help of working code.  You may be reminded of the Agile Manifesto's "[We value] working code over comprehensive documentation."  I prefer to not think of "Documentation" and "Code" as separate things, ideally your well written code should be the documentation that on-boarders flock to as the begin to understand the system they're working on.  In rare cases (ahem, React Redux), this isn't possible, it's too complex, and I do write notes to myself and others when things get hairy, but ideally when I'm coming up to speed on a project, I go right to the code and don't even check how out-of-date the wikis documentation is.

With those values guiding me, I realized, this whole project could be automated with a single python script.  Instead of instructing the user to

- find their pogo on the network
- configure their ssh connections
- run various commands to flash their NAND

We can perform all of these tasks in a piece of Python code.  

(On Laptop)
```
git clone
cd edu-demo-pogoplug-linux
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt

python3 remote_install.py
```

Now run `reboot`.  

```
reboot now
```

When your Pogo Plug comes back online, nothing will have changed really, but you'll have the 2012 version of UBoot.  But you need to perform a follow-up upgrade.  I didn't want to refactor the script to install the latest version of UBoot.  

Now the next time you reboot, the bootloader will search for a flash drive with a single ext4 partition on it with a volume label of "rootfs."  Let's set that partition up next.  


## Put Linux on USB Drive

If this is your first time formatting a USB stick, I'm going to refer you to google because if you follow my instructions and choose the wrong device, you could wind up inadvertently wiping out all the data on your hard drive!  

But if you are comfortable identifying your USB drive, then here's some notes on formatting your Linux USB Drive with an ext4 filesystem (I used a Debian VM on Proxmox because I don't trust my mac with ext4 partitions).  

```
# Format the usb stick, deleting it's contents
# Find the /dev/TARGET
sudo fdisk -l

# Set your target here... e.g. if your flash drive was /dev/sdb, then put sdb here
TARGET=sdb

# Begin the formatting
sudo fdisk /dev/${TARGET}
# choices: o, n, p, 1, ENTER, ENTER, w
# Create file system
sudo mke2fs -t ext4 -L rootfs -j /dev/${TARGET}1
```

Once you have a drive properly formatted, you're ready to just drop in the linux files, no need to use any special tooling to write boot sector data, that part was handled when we upgraded UBoot.

```
#####################################
# Download Linux onto the USB drive #
#####################################
mkdir /mnt/usb
sudo chown $USER /mnt/usb
sudo mount /dev/${TARGET}1

cd /mnt/usb
wget https://bit.ly/3sgEUVj -O Debian-6.5.7-kirkwood-tld-1-rootfs-bodhi.tar.bz2
sha256sum Debian-6.5.7-kirkwood-tld-1-rootfs-bodhi.tar.bz2
# assert 04a2dba61dc3284fbbfb5ac9b439655f2fc44293cd228dce2b774d37b4512fd3
sudo tar -xjf Debian-6.5.7-kirkwood-tld-1-rootfs-bodhi.tar.bz2
sudo rm Debian-6.5.7-kirkwood-tld-1-rootfs-bodhi.tar.bz2
sync
cd ..
sudo umount /mnt/usb
```

Now you're ready to plug this drive into your Pogo Plug and boot up Debian.  The username is root and so is the password.  From there it's just a matter of system administration to get this device to do what you want it.  I recommend vsFTPd, nginx.  And don't forget that file transfers will be significatnly slower if using encryption, so it's probably best to confine this thing to your local LAN.
